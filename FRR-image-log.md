14 August 2018
- Start with UBUNTU18.04-STD image
- Delete (some of) X11
  - apt remove x11-common emacs25 libice6 && apt autoremove
- Upgrade packages
  - Run apt update && apt upgrade
  - Run apt clean && apt autoremove && apt autoclean
  - Don't forget to put grub on the first *partition*
- Install Python virtualenv
  - sudo apt install virtualenv
- Remove old kernel version
  - (Should reboot the system into the new kernel first otherwise the removal messes up)
  - sudo apt remove --purge linux-image-4.15.0-23-generic linux-headers-4.15.0-23*
- Install FRR from https://frrouting.org/ version 5.0.1
- Disable FRR service `systemctl disable frr`

15 March 2019
- Upgrade packages
  - Run apt update && apt upgrade
  - Run apt clean && apt autoremove && apt autoclean
- Remove old kernel
  - Reboot
  - `apt remove --purge linux-image-4.15.0-30-generic linux-headers-4.15.0-30*`
    - For some reason, had to also run `rm -rf /lib/modules/4.15.0-30-generic/`
- Install IPT_NETFLOW version 2.3
  - I couldn't find a package; built from source
  - `wget https://github.com/aabc/ipt-netflow/archive/v2.3.tar.gz`
  - tar -xvf v2.3.tar.gz
  - `sudo apt install iptables-dev pkg-config snmpd libsnmp-dev dkms`
  - ./configure
  - make
  - sudo make all install
  - Add `dlmod netflow /usr/lib/snmp/dlmod/snmp_NETFLOW.so` to `/etc/snmp/snmpd.conf`

10 April 2019
 - Install SiLK
   - See segment-routing-orchestrator/BUILDING-SILK.md for steps taken
 - Upgrade packages (same steps as March 15)
 - Remove old kernel
   - `apt remove --purge linux-image-4.15.0-46-generic linux-headers-4.15.0-46*`
   - `rm -rf /lib/modules/4.15.0-46-generic/`