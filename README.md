To clone this repository, do ```git clone --recurse-submodules -j8 git@gitlab.flux.utah.edu:safeedge/demo-capsule.git```

If you have already cloned this repository without the --recurse-submodules command, you can run ```git submodule update --init --recursive``` to set up the repository

## Demo Topology
The scripts and services contained in the sub repositories of this repository are
designed to be run on any topology given an appropriately-formatted NetJSON NetworkGraph.

However, this repository also contains an emulab experiment on which to demo the
package. The demo topology makes few assumptions, but one is that "customer" devices
are assigned to run OSPF. This is purely for convenience of route assignment.
In a real topology, it might make sense to run something like DHCP, or just manually
configure all nodes

## NetworkGraph
Please see README.md in the orchestrator repository