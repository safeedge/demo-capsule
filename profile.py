"""
This is an Emulab profile for generating a three-level star topology where core routers run Free Range Routing
"""

import geni.portal as portal
import geni.rspec.pg as pg
import geni.rspec.igext as igext

pc = portal.Context()

pc.defineParameter("osNodeType", "Hardware Type",
                   portal.ParameterType.NODETYPE, "",
                   longDescription="A specific hardware type to use for each node.  Cloudlab clusters all have machines of specific types.  When you set this field to a value that is a specific hardware type, you will only be able to instantiate this profile on clusters with machines of that type.  If unset, when you instantiate the profile, the resulting experiment may have machines of any available type allocated.")
pc.defineParameter(name="num_cores",
                   description="Number of core routers",
                   typ=portal.ParameterType.INTEGER,
                   defaultValue=4,
                   )
pc.defineParameter(name="lvl1_fanout",
                   description="Number of level 1 routers connect to each core router",
                   typ=portal.ParameterType.INTEGER,
                   defaultValue=3,
                   )
pc.defineParameter(name="core_name",
                   description="Base name for the core routers",
                   typ=portal.ParameterType.STRING,
                   defaultValue="core",
                   )
pc.defineParameter(name="lvl1_name",
                   description="Base name for the level 1 routers",
                   typ=portal.ParameterType.STRING,
                   defaultValue="lvl1",
                   )
pc.defineParameter(name="ovs_name",
                   description="Base name for the edge OVS switches",
                   typ=portal.ParameterType.STRING,
                   defaultValue="ovs")
pc.defineParameter(name="host_name",
                   description="Base name for host nodes",
                   typ=portal.ParameterType.STRING,
                   defaultValue="host")
pc.defineParameter(name="core_if_name",
                   description="Base name for interfaces which connect to core routers",
                   typ=portal.ParameterType.STRING,
                   defaultValue="core_if")
pc.defineParameter(name="lvl1_if_name",
                   description="Base name for interfaces which connect to level 1 routers",
                   typ=portal.ParameterType.STRING,
                   defaultValue="lvl1_if")
pc.defineParameter(name="core_net_name",
                   description="Base name for a network which contains only core routers",
                   typ=portal.ParameterType.STRING,
                   defaultValue="lan")
pc.defineParameter(name="lvl1_net_name",
                   description="Base name for a network which connects core and level1 routers",
                   typ=portal.ParameterType.STRING,
                   defaultValue="lan")
pc.defineParameter(name="edge_net_name",
                   description="Base name for a network which connects a router to an edge (ovs) switch",
                   typ=portal.ParameterType.STRING,
                   defaultValue="edgelan")
pc.defineParameter(name="host_net_name",
                   description="Base name for a network which connects an edge switch to a host",
                   typ=portal.ParameterType.STRING,
                   defaultValue="hostlan")

params = pc.bindParameters()

image_urn = 'emulab.net'
image_project = 'SafeEdge'
image_os = 'Ubuntu_18.04'
image_tag = 'FRR:3'

router_disk_image = 'urn:publicid:IDN+{urn}+image+{proj}:{os}_{tag}'.format(urn=image_urn, proj=image_project, os=image_os, tag=image_tag)
edge_switch_disk_image = router_disk_image

request = pc.makeRequestRSpec()

node_constructor = request.RawPC

# Add the core nodes to the topology
cores = []
for core1_idx in range(params.num_cores):
    core = node_constructor("{}{}".format(params.core_name, core1_idx))
    core.addNamespace(pg.Namespaces.EMULAB)

    core.disk_image = router_disk_image
    core.hardware_type = params.osNodeType

    cores.append(core)

# Interconnect all core routers
for core1_idx in range(params.num_cores):
    core1 = cores[core1_idx]
    # The name of the interface on the core2 router which connects to core1
    core2_interface_name = "{}{}".format(params.core_if_name, core1_idx)

    for core2_idx in range(core1_idx + 1, params.num_cores):
        core2 = cores[core2_idx]

        # The name of the interface on the core1 router which connects to core2
        core1_interface_name = "{}{}".format(params.core_if_name, core2_idx)

        core1_interface = core1.addInterface(core1_interface_name)
        core2_interface = core2.addInterface(core2_interface_name)

        link = request.LAN("lan-{}-{}".format(core1.name, core2.name))

        link.addInterface(core1_interface)
        link.addInterface(core2_interface)

# Build level 1 ring routers
lvl1_routers = []
for core_idx in range(len(cores)):
    core = cores[core_idx]
    for lvl1_idx in range(0, params.lvl1_fanout):
        router = node_constructor("{prefix}-{lvl1}-{core}".format(prefix=params.lvl1_name, lvl1=lvl1_idx, core=core_idx))

        router.disk_image = router_disk_image
        router.hardware_type = params.osNodeType

        # Connect this router to its core
        core_interface_name = "{prefix}{number}".format(prefix=params.lvl1_if_name, number=lvl1_idx)
        router_interface_name = "{prefix}{number}".format(prefix=params.core_if_name, number=core_idx)

        core_interface = core.addInterface(core_interface_name)
        router_interface = router.addInterface(router_interface_name)

        link = request.LAN("{prefix}-{core}-{lvl1}".format(prefix=params.lvl1_net_name, lvl1=lvl1_idx, core=core_idx))

        link.addInterface(core_interface)
        link.addInterface(router_interface)

        lvl1_routers.append(router)

# Alias in case I want to add a third ring layer
edge_routers = lvl1_routers

# Build OVS routers on the outside layer of the ring
ovs_nodes = []
for edge_idx in range(len(edge_routers)):
    edge = edge_routers[edge_idx]

    ovs = node_constructor("{prefix}-{edge}".format(prefix=params.ovs_name, edge=edge.client_id))

    ovs.disk_image = edge_switch_disk_image
    ovs.hardware_type = params.osNodeType

    ovs_interface_name = "edge{number}".format(number=edge_idx)
    edge_interface_name = "ovs{number}".format(number=edge_idx)

    ovs_interface = ovs.addInterface(ovs_interface_name)
    edge_interface = edge.addInterface(edge_interface_name)

    link = request.LAN("{prefix}-{number}".format(prefix=params.edge_net_name, number=edge_idx))

    link.addInterface(ovs_interface)
    link.addInterface(edge_interface)

    ovs_nodes.append(ovs)

# Build hosts on the outside of the OVS nodes
for index in range(0, len(ovs_nodes)):
    ovs = ovs_nodes[index]

    host = node_constructor("{prefix}-{ovs}".format(prefix=params.host_name, ovs=ovs.client_id))

    # I am going to allow the "customer hosts" to run FRR/OSPF purely for simplicity of assigning routes
    # In a real setup, this would be handled by DHCP or similar, maybe running on the same nodes as OVS
    host.disk_image = router_disk_image
    host.hardware_type = params.osNodeType

    host_interface_name = "ovs{number}".format(number=index)
    ovs_interface_name = "host{number}".format(number=index)

    host_interface = host.addInterface(host_interface_name)
    ovs_interface = ovs.addInterface(ovs_interface_name)

    link = request.LAN("{prefix}-{number}".format(prefix=params.host_net_name, number=index))

    link.addInterface(host_interface)
    link.addInterface(ovs_interface)

pc.printRequestRSpec(request)